@apikey
Feature: Validating the REST scenarios which uses APIKey for authenticating

  @regression
  Scenario Outline: Validating the whole json response
    Given I map this project API Key on the headers
    And I form a client with this resource url "<ResourcePath>"
    Then I make a GET call with API Key and capture the response
    And I get the "response" payload from "<ResponseFile>" file for the scenario "<Scenario>"
    Then custom step to remove the dynamic json key generation
    Then I validate the output response with expected data
    And I verify the HTTP code is <HTTPCode>

    Examples:
      | ResourcePath                 | ResponseFile     | Scenario       | HTTPCode |
      | /v1/cryptocurrency/info?id=1 | APIKey_Responses | User_Scenario1 | 200      |

  @regression
  Scenario Outline: Validating the specific json elements
    Given I map this project API Key on the headers
    And I form a client with this resource url "<ResourcePath>"
    Then I make a GET call with API Key and capture the response
    Then I form a JsonPath and verify the output object with ExpectedObject:
      | JsonPath             | ExpectedObject  |
      | status.error_code    | <error_code>    |
      | status.error_message | <error_message> |
    And I verify the HTTP code is <HTTPCode>

    Examples:
      | ResourcePath                    | HTTPCode | error_code | error_message                                           |
      | /v1/cryptocurrency/info?id=5000 | 400      | 400        | Invalid value for "id": "5000"                          |
      | /v1/cryptocurrency/info         | 400      | 400        | "value" must contain at least one of [id, symbol, slug] |
