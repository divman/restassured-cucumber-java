@oauth
Feature: Validating the REST scenarios which uses OAuth2.0 for authenticating

  @regression
  Scenario: Generating OAuth token for all scenarios under this scenario
    Given I get the "Request" payload from "OAuth_Requests" file for the scenario "Token_Gen"
    And I generate OAuth token with resource "/auth/oauth2/v2/token"

  @regression
  Scenario Outline: Validating the whole json response
    Given I form a client with this resource url "<ResourcePath>"
    Then I make a GET call with OAuth token and capture the response
    And I get the "response" payload from "<ResponseFile>" file for the scenario "<Scenario>"
    Then I validate the output response with expected data
    And I verify the HTTP code is <HTTPCode>

    Examples:
      | ResourcePath | ResponseFile    | Scenario       | HTTPCode |
      | /api/1/users | OAuth_Responses | User_Scenario1 | 200      |
      | /api/1/user  | OAuth_Responses | User_Scenario2 | 404      |

  @regression
  Scenario Outline: Validating the specific json elements
    Given I form a client with this resource url "<ResourcePath>"
    Then I make a GET call with OAuth token and capture the response
    Then I form a JsonPath and verify the output object with ExpectedObject:
      | JsonPath             | ExpectedObject |
      | data[0].firstname    | Sdet           |
      | data[0]['firstname'] | SDET           |
    And I verify the HTTP code is <HTTPCode>

    Examples:
      | ResourcePath | HTTPCode |
      | /api/1/users | 200      |
