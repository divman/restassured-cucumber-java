package Steps;

import Utilities.RESTUtil;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.skyscreamer.jsonassert.JSONAssert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

public class ScenarioSteps extends BaseSteps {

    private String jsonText = null;
    private JsonPath APIResponse = null;
    private String strResponse = null;
    private String strURL = null;
    private String actJsonValue = "";

    private final RESTUtil restUtil;

    public ScenarioSteps() {
        restUtil = new RESTUtil();
        restUtil.setUpService();
    }

    @And("^I get the \"(.*?)\" payload from \"(.*?)\" file for the scenario \"(.*?)\"$")
    public void get_the_payload_from_respective_folders(String folderName, String fileName, String scenarioKey) throws Throwable {
        if (folderName.equalsIgnoreCase("Request")) {
            fileName = "Requests/" + fileName;
        } else if (folderName.equalsIgnoreCase("Response")) {
            fileName = "Responses/" + fileName;
        }

        HashMap<String, Object> map = yamlConversion.getYAMLContent(fileName);
        JSONObject jsonObject = (JSONObject) yamlConversion.YMLtoJSON(map);
        if (fileName.contains("Request")) {
            JSONObject ipJsonObject = (JSONObject) jsonObject.get(scenarioKey);
            jsonText = ipJsonObject.toString();
        } else if (fileName.contains("Response")) {
            JSONObject expJsonObject = (JSONObject) jsonObject.get(scenarioKey);
            jsonText = expJsonObject.toString();
        }
        scenario.write("jsonText: " + jsonText);
    }

    @Given("^I generate OAuth token with resource \"(.*?)\"$")
    public void i_generate_OAuth_token_with_resource(String resourceURL) throws Throwable {
        HashMap<String, String> secrets = (HashMap<String, String>) base_config.get("OAuth_Secrets");
        String fullSecret = "client_id:" + secrets.get("client_id") + ", " + "client_secret:" + secrets.get("client_secret");
        commonHeaders.put("Authorization", fullSecret);
        restUtil.setUpRequest(commonHeaders);
        System.out.println("commonHeaders is : " + commonHeaders);

        APIResponse = restUtil.POSTResponse(resourceURL, jsonText);
        String access_token = APIResponse.getString("access_token");

        propertyConfig.setProperty("token", access_token);
        strResponse = APIResponse.prettify();
        scenario.write("API_Response is : " + strResponse);
    }

    @And("^I form a client with this resource url \"(.*?)\"$")
    public void I_form_a_client_with_this_resource_url(String resourceURL) throws Throwable {
        restUtil.setUpRequest(commonHeaders);
        strURL = resourceURL;
    }

    @And("^I map this project API Key on the headers$")
    public void I_map_this_project_API_Key_on_the_headers() {
        HashMap<String, String> secrets = (HashMap<String, String>) base_config.get("API_Key");
        /*for (Map.Entry pair : secrets.entrySet()) {
            commonHeaders.put(pair.getKey().toString(), pair.getValue().toString());
        }*/
        commonHeaders.putAll(secrets);
    }

    @When("^I make a GET call with OAuth token and capture the response$")
    public void I_make_a_GET_call_OAuth_token_and_capture_the_response() throws Throwable {
        Properties prop = propertyConfig.loadProperty();
        String access_token = prop.getProperty("token");
        APIResponse = restUtil.GETResponseWithOAuth(strURL, access_token);
        strResponse = APIResponse.prettify();
        scenario.write("API_Response is : " + strResponse);
    }

    @When("^I make a GET call with API Key and capture the response$")
    public void I_make_a_GET_call_API_Key_and_capture_the_response() throws Throwable {
        APIResponse = restUtil.GETResponseWithAPIKey(strURL);
        strResponse = APIResponse.prettify();
        scenario.write("API_Response is : " + strResponse);
    }

    @Then("^I validate the output response with expected data$")
    public void I_validate_the_output_response_with_expected_data() throws Throwable {
        JSONAssert.assertEquals(jsonText, strResponse, false);
    }

    @And("^I verify the HTTP code is (\\d+)$")
    public void I_verify_the_HTTP_error_code_is(int statusCode) throws Throwable {
        scenario.write("The HTTP code status line : " + restUtil.getStatusLine());
        assertThat(restUtil.getStatusCode()).isEqualTo(statusCode);
    }

    @Then("custom step to remove the dynamic json key generation")
    public void customStepToRemoveTheDynamicJsonKeyGeneration() throws Throwable {
        JSONObject obj = (JSONObject) JSONValue.parse(restUtil.getResponseAsString());
        strResponse = obj.get("data").toString();
        scenario.write("API_Response is : " + strResponse);
    }

    @Then("I form a JsonPath and verify the output object with ExpectedObject:")
    public void iFormAJsonPathAndVerifyTheOutputObjectWithExpectedObject(DataTable caseData) throws Throwable {
        List<Map<String, String>> getJsonPath = caseData.asMaps(String.class, String.class);
        for (Map<String, String> table : getJsonPath) {
            String jsonPath = table.get("JsonPath");
            String expJsonObject = table.get("ExpectedObject");
            actJsonValue = APIResponse.get(jsonPath).toString();
            assertThat(expJsonObject).isEqualTo(actJsonValue);
        }
    }
}
