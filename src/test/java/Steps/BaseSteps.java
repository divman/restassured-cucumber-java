package Steps;

import Utilities.PropertyConfig;
import Utilities.YAMLConversion;
import io.cucumber.java.Scenario;

import java.util.HashMap;

public class BaseSteps {

    public static Scenario scenario;
    public static YAMLConversion yamlConversion;
    public static PropertyConfig propertyConfig;
    public static HashMap<String, String> commonHeaders;
    public static HashMap<String, Object> base_config;

    public static void RESTSetup(Scenario scenario) {
        BaseSteps.scenario = scenario;

        try {
            yamlConversion = new YAMLConversion();
            propertyConfig = new PropertyConfig();
            base_config = yamlConversion.getYAMLContent("Properties/restconfig_properties");
            commonHeaders = (HashMap<String, String>) base_config.get("Common_Headers");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
