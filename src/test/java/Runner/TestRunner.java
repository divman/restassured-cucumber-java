package Runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

// mvn clean install -Dcucumber.options="--tags @regression"

@CucumberOptions(
        features = {"src/test/resources/Features/"},
        glue = {"Steps"},
        plugin = {
                "pretty",
                "html:target/site/cucumber-pretty",
                "json:target/cucumber.json"
        }
)

public class TestRunner extends AbstractTestNGCucumberTests {

}
