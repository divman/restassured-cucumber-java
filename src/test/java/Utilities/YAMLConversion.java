package Utilities;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YAMLConversion {

    public Map<String, Object> JsonToMap(JSONObject json) {
        Map<String, Object> retMap = new HashMap<>();

        if (!json.isEmpty()) {
            retMap = toMap(json);
        }
        return retMap;
    }

    private static Map<String, Object> toMap(JSONObject object) {
        Map<String, Object> map = new HashMap<>();

        for (Object jKey : object.keySet()) {
            String key = (String) jKey;
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }

            /*if ((MonteTransactions.listFromCardProperty).contains(key)) {
                map.put(key, value);
            }*/

            map.put(key, value);
        }
        /*Iterator<String> keysItr = (Iterator<String>) object.keySet();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }


            map.put(key, value);
        }*/
        return map;
    }

    private static List<Object> toList(JSONArray array) {
        List<Object> list = new ArrayList<>();

        for (Object value : array) {
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }

        return list;
    }

    private static Iterable<Object> loadAll(String fileName) throws IOException, URISyntaxException {
        URL urlConfigFile = YAMLConversion.class.getClassLoader().getResource("Properties/" + fileName + ".yml");
        File strFile = new File(urlConfigFile.toURI());
        FileInputStream istream = new FileInputStream(strFile);
        Yaml yaml = new Yaml();
        Iterable<Object> itrObj;
        try (InputStream in = istream) {
            itrObj = yaml.loadAll(in);
            /*for (Object obj : itrObj) {
                map = (Map<String, List<String>>) obj;
            }*/
        }
        return itrObj;
    }

    public Map<String, Map<String, String>> YMLtoMAP(String fileName) throws IOException, URISyntaxException {
        Map<String, Map<String, String>> map = null;
        /*URL urlConfigFile = YAMLConversion.class.getClassLoader().getResource("Properties/" + fileName + ".yml");
        File strFile = new File(urlConfigFile.toURI());
        FileInputStream istream = new FileInputStream(strFile);
        Yaml yaml = new Yaml();*/
        //try (InputStream in = istream) {
        //Iterable<Object> itrObj = yaml.loadAll(in);
        Iterable<Object> itrObj = loadAll(fileName);
        for (Object obj : itrObj) {
            map = (Map<String, Map<String, String>>) obj;
        }
        //}
        return map;
    }

    public List<String> YMLtoList(String fileName, String listForKey) throws IOException, URISyntaxException {
        Map<String, List<String>> map = null;
        /*URL urlConfigFile = YAMLConversion.class.getClassLoader().getResource("Properties/" + fileName + ".yml");
        File strFile = new File(urlConfigFile.toURI());
        FileInputStream istream = new FileInputStream(strFile);
        Yaml yaml = new Yaml();*/
        //try (InputStream in = istream) {
        //Iterable<Object> itrObj = yaml.loadAll(in);
        Iterable<Object> itrObj = loadAll(fileName);
        for (Object obj : itrObj) {
            map = (Map<String, List<String>>) obj;
        }
        //}
        return map != null ? map.get(listForKey) : null;
    }

    public HashMap<String, Object> getYAMLContent(String fileName) throws FileNotFoundException, URISyntaxException {
        Yaml yaml = new Yaml();
        URL urlConfigFile = YAMLConversion.class.getClassLoader().getResource(fileName + ".yml");
        File strFile = new File(urlConfigFile.toURI());
        FileInputStream istream = new FileInputStream(strFile);
        return (HashMap<String, Object>) yaml.load(istream);
    }

    @SuppressWarnings("unchecked")
    public Object YMLtoJSON(Object obj) {
        if (obj instanceof Map) {
            Map<Object, Object> map = (Map<Object, Object>) obj;
            JSONObject result = new JSONObject();
            for (Map.Entry<Object, Object> stringObjectEntry : map.entrySet()) {
                String key = stringObjectEntry.getKey().toString();
                result.put(key, YMLtoJSON(stringObjectEntry.getValue()));
            }
            return result;
        } else if (obj instanceof ArrayList) {
            ArrayList<?> arrayList = (ArrayList<?>) obj;
            JSONArray result = new JSONArray();
            for (Object arrayObject : arrayList) {
                result.add(YMLtoJSON(arrayObject));
            }
            return result;
        } else if (obj instanceof String) {
            return obj;
        } else if (obj instanceof Boolean) {
            return obj;
        } else if (obj instanceof Integer) {
            return obj;
        } else if (obj == null) {
            return obj;
        } else {
            System.out.println("Unsupported class : " + obj.getClass().getName());
            //logger.error("Unsupported class [{0}]", obj.getClass().getName());
            return obj;
        }
    }
}
