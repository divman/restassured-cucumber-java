package Utilities;

import static io.restassured.RestAssured.*;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;
//import java.util.Properties;

public class RESTUtil {

    //private Properties systemProperties;
    private Response response = null;
    public Map<String, Object> rest_config = null;
    private HashMap<String, String> mapReqHeader = new HashMap<>();
    public String strBaseUrl;
    private YAMLConversion yamlConversion;

    public RESTUtil() {
        yamlConversion = new YAMLConversion();
        try {
            rest_config = yamlConversion.getYAMLContent("Properties/restconfig_properties");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setUpService() {
        try {
            //systemProperties = System.getProperties();
            //systemProperties.setProperty("http.proxyHost", "<HOST>");
            //systemProperties.setProperty("http.proxyPort", "<PORT>");
            Map<String, String> map = (Map<String, String>) rest_config.get("Base");
            strBaseUrl = map.get("baseURL");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setUpRequest(HashMap<String, String> mapHeader) {
        mapReqHeader = mapHeader;
    }

    public JsonPath POSTResponse(String serviceURL, String jsonRequest) {
        response = given().
                headers(mapReqHeader).
                body(jsonRequest).
                when().
                post(strBaseUrl + serviceURL);

        return response.jsonPath();
    }

    public JsonPath GETResponseWithOAuth(String serviceURL, String token) {
        //response = given().log().all().headers(mapReqHeader).urlEncodingEnabled(false).when().get(strBaseUrl + serviceURL);
        response = given().
                headers(mapReqHeader).
                auth().
                oauth2(token).
                when().
                get(strBaseUrl + serviceURL);

        return response.jsonPath();
    }

    public JsonPath GETResponseWithAPIKey(String serviceURL) {
        response = given().
                headers(mapReqHeader).
                when().
                get(strBaseUrl + serviceURL);

        return response.jsonPath();
    }

    public int getStatusCode() {
        return response.getStatusCode();
    }

    public String getStatusLine() {
        return response.getStatusLine();
    }

    public String getResponseAsString() {
        return response.asString();
    }
}
