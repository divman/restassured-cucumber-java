package Utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertyConfig {

    protected Properties properties;
    protected String strFile;

    public PropertyConfig() {
        properties = new Properties();
        strFile = "src/test/resources/Properties/temp.properties";
    }

    public Properties loadProperty() throws Exception {
        try (InputStream inputStream = new FileInputStream(strFile)) {
            properties.load(inputStream);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
        return properties;
    }

    public void setProperty(String key, String value) throws Exception {
        try (FileInputStream inputStream = new FileInputStream(strFile)) {
            properties.load(inputStream);
            properties.put(key, value);
            inputStream.close();

            FileOutputStream output = new FileOutputStream(strFile);
            properties.store(output, "This is overwrite file - Do not delete the 'token' key from this property file.");
            output.close();
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }
}
